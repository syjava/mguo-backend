package com.mguo.backend.common;

/**
 * 类MguoException的实现描述：TODO 类实现描述
 *
 * @author sunyj 2022/4/26 20:51
 */
public class MguoException extends RuntimeException  {


        private String code;

        public MguoException(String code) {
            this.code = code;
        }

        public MguoException(String code, Object message) {
            super(message.toString());
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

}
