//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.mguo.backend.common;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;


@JsonSerialize(
        include = Inclusion.NON_NULL
)
public class Response<T> implements Serializable {
    private static final long serialVersionUID = -4505655308965878999L;
    private static final String successCode = "0";

    private T data;
    /**
     * 错误代码，0--成功，其他失败
     */
    private String code = "0";

    private Object msg = "success";

    private Response() {
    }

    private Response success(T data) {
        this.data = data;
        return this;
    }

    private Response fail(String code, Object msg) {
        this.code = code;
        this.msg = msg;
        return this;
    }

    public static Response builderSuccess(Object o) {
        return (new Response()).success(o);
    }

    public static Response builderFail(String code, Object msg) {
        return (new Response()).fail(code, msg);
    }

    public T getData() {
        return this.data;
    }

    public String getCode() {
        return this.code;
    }

    public Object getMsg() {
        return this.msg;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public boolean equals(Object o) {
        if(o == this) {
            return true;
        } else if(!(o instanceof Response)) {
            return false;
        } else {
            Response other = (Response)o;
            if(!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$data = this.getData();
                    Object other$data = other.getData();
                    if(this$data == null) {
                        if(other$data == null) {
                            break label47;
                        }
                    } else if(this$data.equals(other$data)) {
                        break label47;
                    }

                    return false;
                }

                String this$code = this.getCode();
                String other$code = other.getCode();
                if(this$code == null) {
                    if(other$code != null) {
                        return false;
                    }
                } else if(!this$code.equals(other$code)) {
                    return false;
                }

                Object this$msg = this.getMsg();
                Object other$msg = other.getMsg();
                if(this$msg == null) {
                    if(other$msg != null) {
                        return false;
                    }
                } else if(!this$msg.equals(other$msg)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof Response;
    }

    public int hashCode() {
        boolean PRIME = true;
        byte result = 1;
        Object $data = this.getData();
        int result1 = result * 59 + ($data == null?43:$data.hashCode());
        String $code = this.getCode();
        result1 = result1 * 59 + ($code == null?43:$code.hashCode());
        Object $msg = this.getMsg();
        result1 = result1 * 59 + ($msg == null?43:$msg.hashCode());
        return result1;
    }

    public String toString() {
        return "Response(data=" + this.getData() + ", code=" + this.getCode() + ", msg=" + this.getMsg() + ")";
    }
}
