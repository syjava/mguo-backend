package com.mguo.backend.common;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.mguo.backend.config.BaseConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public  class  PastUtil {
    public  static  String token =  null ;
    public  static  String time =  null ;
    public  static  String jsapi_ticket =  null ;

    public static void init(){
        token = CommonUtil.getToken(BaseConfig.APP_ID, BaseConfig.APP_SECRET);
        jsapi_ticket = CommonUtil.getJsApiTicket(token);
        time = getTime();
        log.error("首次启动获取token"+token +";获取jsapi_ticket" +jsapi_ticket);
    }
    /**
     * @param url
     * @return
     */
    public  static   Map<String, String> getParam(String url){
        if (token ==  null ){
             token = CommonUtil.getToken(BaseConfig.APP_ID, BaseConfig.APP_SECRET);
            jsapi_ticket = CommonUtil.getJsApiTicket(token);
            time = getTime();
        } else {
            if (!time.substring( 0 ,  13 ).equals(getTime().substring( 0 ,  13 ))){  //每小时刷新一次
                token =  null ;
                token = CommonUtil.getToken(BaseConfig.APP_ID, BaseConfig.APP_SECRET);
                jsapi_ticket = CommonUtil.getJsApiTicket(token);
                time = getTime();
            }
        }
        Map<String, String> params = sign(jsapi_ticket, url);
        params.put( "appid" , BaseConfig.APP_ID);
        return  params;
    }

    public  static  Map<String, String> sign(String jsapi_ticket, String url) {
        Map<String, String> ret =  new HashMap<String, String>();
        String nonce_str = create_nonce_str();
        String timestamp = create_timestamp();
        String str;
        String signature =  "" ;

        //注意这里参数名必须全部小写，且必须有序
        str =  "jsapi_ticket="  + jsapi_ticket +
                "&noncestr="  + nonce_str +
                "&timestamp="  + timestamp +
                "&url="  + url;

        try
        {
            MessageDigest crypt = MessageDigest.getInstance( "SHA-1" );
            crypt.reset();
            crypt.update(str.getBytes( "UTF-8" ));
            signature = byteToHex(crypt.digest());
        }
        catch  (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch  (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        ret.put( "url" , url);
        ret.put("string1",str);
        ret.put( "jsapi_ticket" , jsapi_ticket);
        ret.put( "nonceStr" , nonce_str);
        ret.put( "timestamp" , timestamp);
        ret.put( "signature" , signature);

        return  ret;
    }

    private  static  String byteToHex( final  byte [] hash) {
        Formatter formatter =  new  Formatter();
        for  ( byte  b : hash)
        {
            formatter.format( "%02x" , b);
        }
        String result = formatter.toString();
        formatter.close();
        return  result;
    }

    private  static  String create_nonce_str() {
        return  UUID.randomUUID().toString();
    }

    private  static  String create_timestamp() {
        return  Long.toString(System.currentTimeMillis() /  1000 );
    }

    //获取当前系统时间 用来判断access_token是否过期
    public  static  String getTime(){
        Date dt= new  Date();
        SimpleDateFormat sdf = new  SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        return  sdf.format(dt);
    }
}
