package com.mguo.backend.common;

import java.util.Date;

/**
 * 类DateUtil的实现描述：时间工具类
 *
 * @author sunyj 2022/4/27 0:03
 */
public class DateUtil {

    /**

     * 获取精确到秒的时间戳

     * @param date

     * @return

     */

    public static Long getSecondTimestamp(Date date){
        if (null == date) {

            return 0L;

        }

        String timestamp = String.valueOf(date.getTime()/1000);

        return Long.valueOf(timestamp);

    }
}
