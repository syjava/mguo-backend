package com.mguo.backend.service.impl;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.mguo.backend.common.MguoException;
import com.mguo.backend.mapper.FileResouseMapper;
import com.mguo.backend.model.FileResouseDO;
import com.mguo.backend.model.FileResouseGroup;
import com.mguo.backend.model.QueueNumDTO;
import com.mguo.backend.service.AudioService;
import com.mguo.backend.service.FileResouseService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * FileResouseService 实现
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
@Slf4j
@Service
public class FileResouseServiceImpl implements FileResouseService {

    @Resource
    private FileResouseMapper fileResouseMapper;
    @Resource
    private AudioService audioService;

    /**
     * 根据id获取
     * @param name
     */
    @Override
    public List<FileResouseGroup> getListByName(String name){
        List<FileResouseGroup> grouplist = new ArrayList<>();
        List<FileResouseDO>  list = fileResouseMapper.getListByName(name);
        if(CollectionUtils.isEmpty(list)){
            return grouplist;
        }

        //分组，考虑部门为空的情况
        Map<String,List<FileResouseDO>> map = new HashMap<>();
        for (FileResouseDO fileResouseDO: list) {
            //key为公司 + 部门
            String key = fileResouseDO.getCompanyName() + "_" + (StringUtils.isBlank(fileResouseDO.getDeptName()) ? "0" : fileResouseDO.getDeptName());
            if(CollectionUtils.isEmpty(map.get(key))){
                List<FileResouseDO> fileResouseDOList = new ArrayList<>();
                fileResouseDOList.add(fileResouseDO);
                map.put(key,fileResouseDOList);
            }else{
                map.get(key).add(fileResouseDO);
            }
        }

        //遍历，构建分组对象
        Iterator<Map.Entry<String, List<FileResouseDO>>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, List<FileResouseDO>> entry = it.next();
            String[] names = entry.getKey().split("_");
            FileResouseGroup fileResouseGroup = new  FileResouseGroup();
            fileResouseGroup.setCompanyName(names[0]);
            fileResouseGroup.setDeptName("0".equals(names[1]) ? null : names[1]);
            fileResouseGroup.setFileResouseDOs(entry.getValue());
            grouplist.add(fileResouseGroup);
        }
        return grouplist;
    }

    /**
     * 根据id获取
     * @param id
     * @return
     */
    @Override
    public FileResouseDO getById(Long id){
        //根据ID查找
        FileResouseDO fileResouseDO = fileResouseMapper.getById(id);
        if(fileResouseDO == null){
            throw new MguoException("0", "资源不存在");
        }

        //获取播放、排队列表
        Map<String, Object> map = audioService.playList();
//        FileResouseDO currentFile = FileResouseDO.class.cast(map.get("current"));
        List<FileResouseDO> queueFileList = JSON.parseArray(JSON.toJSONString(map.get("list")),FileResouseDO.class);
        //设置排队数量
        fileResouseDO.setQueueCount(CollectionUtils.isEmpty(queueFileList) ? 0 : queueFileList.size());

        //判断是否为正在播放的
//        if(currentFile != null && currentFile.getId().equals(fileResouseDO.getId())){
//            //如果资源正在播放，则不能点击播放
//            fileResouseDO.setCanPlay(false);
//            return fileResouseDO;
//        }

        //判断是否在列表中
        if(CollectionUtils.isNotEmpty(queueFileList)){
            for (FileResouseDO file : queueFileList) {
                if(file.getId().equals(fileResouseDO.getId())){
                    fileResouseDO.setCanPlay(false);
                    return fileResouseDO;
                }
            }
        }

        QueueNumDTO queueNumDTO = audioService.getQueueSize();
        //判断总数是否大于阈值
        int count = CollectionUtils.isEmpty(queueFileList) ? 0 : queueFileList.size();
        if(count >= queueNumDTO.getMaxQueueSize()){
            fileResouseDO.setCanPlay(false);
        }else {
            fileResouseDO.setCanPlay(true);
        }
        return fileResouseDO;
    }
}
