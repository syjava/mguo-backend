package com.mguo.backend.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mguo.backend.common.AlikAssert;
import com.mguo.backend.mapper.DeptInfoMapper;
import com.mguo.backend.mapper.PlayListMapper;
import com.mguo.backend.model.DeptInfoDO;
import com.mguo.backend.model.PlayListDO;
import com.mguo.backend.service.PlayListService;

import lombok.extern.slf4j.Slf4j;

/**
 * PlayListService 实现
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
@Slf4j
@Service
public class PlayListServiceImpl implements PlayListService {

    @Resource
    private PlayListMapper playListMapper;

    @Resource
    private DeptInfoMapper deptInfoMapper;


    @Override
    public boolean addNew(Long resourceId, Long deptId) {
        //先判断部门是否存在
        DeptInfoDO deptInfoDO = deptInfoMapper.getById(resourceId);
        AlikAssert.isNotNull(deptInfoDO, "MG_01","部门信息不存在");
        //判断资源是否存在

        PlayListDO pdo = new PlayListDO();
        pdo.setDeptInfoId(deptId);
        pdo.setResourceId(resourceId);
        //查询是否有正在播放中的
        PlayListDO runningPlay = playListMapper.getRunningPlay();
        if(runningPlay == null) {
            //写入进行中
            pdo.setPlayStatus(2);
        }else{
            //写入待点播
            pdo.setPlayStatus(1);
        }

        return  true;

    }
}
