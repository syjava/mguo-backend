package com.mguo.backend.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mguo.backend.mapper.DeptInfoMapper;
import com.mguo.backend.model.DeptInfoDO;
import com.mguo.backend.service.DeptInfoService;

import lombok.extern.slf4j.Slf4j;

/**
 * DeptInfoService 实现
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
@Slf4j
@Service
public class DeptInfoServiceImpl implements DeptInfoService {

    @Resource
    private DeptInfoMapper deptInfoMapper;


    @Override
    public List<DeptInfoDO> list(String content) {
       return deptInfoMapper.list(content);
    }

    @Override
    public List<DeptInfoDO> listCompany() {
        return deptInfoMapper.listCompany();
    }
}
