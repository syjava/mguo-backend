package com.mguo.backend.service;

/**
 * PlayListService 接口
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
public interface PlayListService {

    /**
     * 点播
     * @param resourceId
     * @param deptId
     * @return
     */
    boolean addNew(Long resourceId, Long deptId);
}
