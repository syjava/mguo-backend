package com.mguo.backend.service;

import java.util.List;

import com.mguo.backend.model.DeptInfoDO;

/**
 * DeptInfoService 接口
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
public interface DeptInfoService {

    /**
     * 根据id获取
     */
    List<DeptInfoDO> list(String content);

    /**
     * 获取公司列表
     * @return
     */
    List<DeptInfoDO> listCompany();
}
