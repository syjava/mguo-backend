package com.mguo.backend.service;

import com.mguo.backend.model.FileResouseDO;
import com.mguo.backend.model.FileResouseGroup;

import java.util.List;

/**
 * FileResouseService 接口
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
public interface FileResouseService {
    /**
     * 根据id获取
     * @param name
     */
    List<FileResouseGroup> getListByName(String name);

    /**
     * 根据id获取
     * @param id
     * @return
     */
    FileResouseDO getById(Long id);

}
