package com.mguo.backend.mapper;

import com.mguo.backend.model.PlayListDO;

/**
 * PlayListMapper
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
public interface PlayListMapper {

    /**
    * 根据id获取
    * @param playListDO
    */
    int saveRecord(PlayListDO playListDO);

    /**
     * 是否有正常播放的
     * @return
     */
    PlayListDO getRunningPlay();

    /**
     * 是否有正在等待的
     * @return
     */
    int getWaitPlay();

}
