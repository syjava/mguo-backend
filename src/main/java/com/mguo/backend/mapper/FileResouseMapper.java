package com.mguo.backend.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.mguo.backend.model.FileResouseDO;

/**
 * FileResouseMapper
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
@Mapper
public interface FileResouseMapper {

    /**
    * 根据id获取
    * @param name
    */
    List<FileResouseDO> getListByName(String name);

    /**
     * 根据id获取
     * @param id
     * @return
     */
    FileResouseDO getById(Long id);

}
