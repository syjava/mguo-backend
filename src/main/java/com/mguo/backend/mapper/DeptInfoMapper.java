package com.mguo.backend.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mguo.backend.model.DeptInfoDO;

/**
 * DeptInfoMapper
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
@Mapper
public interface DeptInfoMapper {

    /**
    * 根据id获取
    */
    List<DeptInfoDO> list(@Param("content") String content);

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    DeptInfoDO getById(Long id);

    /**
     * 获取公司列表
     * @return
     */
    List<DeptInfoDO> listCompany();

}
