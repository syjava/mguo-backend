package com.mguo.backend.controller;

import com.mguo.backend.common.MguoException;
import com.mguo.backend.common.Response;
import com.mguo.backend.model.FileResouseDO;
import com.mguo.backend.model.FileResouseGroup;
import com.mguo.backend.service.FileResouseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * TODO
 *
 * @Author: Nyj
 * @Date: 2022/4/26 21:37
 */
@RestController
@RequestMapping("/api/mguo/file")
@Slf4j
@Api(tags = {"资源信息"})
public class FileResouseController {
    @Resource
    private FileResouseService fileResouseService;

    @ApiOperation(value = "获取资源列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping(value = "/listFile")
    public Response listDept(String name) {
        try {
            List<FileResouseGroup> list = fileResouseService.getListByName(name);
            return Response.builderSuccess(list);
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

    @ApiOperation(value = "资源详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping(value = "/getDetail")
    public Response listDept(@RequestParam("id") Long id) {
        try {
            FileResouseDO fileResouseDO = fileResouseService.getById(id);
            return Response.builderSuccess(fileResouseDO);
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }
}
