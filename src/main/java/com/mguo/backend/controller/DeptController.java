package com.mguo.backend.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mguo.backend.common.MguoException;
import com.mguo.backend.common.Response;
import com.mguo.backend.model.DeptInfoDO;
import com.mguo.backend.service.DeptInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * 类DeptController的实现描述：TODO 类实现描述
 *
 * @author sunyj 2022/4/26 20:46
 */
@RestController
@RequestMapping("/api/mguo/dept")
@Slf4j
@Api(tags = {"部门信息获取"})
public class DeptController {

    @Resource
    private DeptInfoService deptInfoService;

    @ApiOperation(value = "获取部门列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping(value = "/listDept")
    public Response listDept(String content) {
        try {
            List<DeptInfoDO> list = deptInfoService.list(content);
            return Response.builderSuccess(list);
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

    @ApiOperation(value = "获取公司列表-去重", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping(value = "/listCompany")
    public Response listCompany() {
        try {
            List<DeptInfoDO> list = deptInfoService.listCompany();
            return Response.builderSuccess(list);
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }
}
