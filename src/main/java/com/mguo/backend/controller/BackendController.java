package com.mguo.backend.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mguo.backend.common.CommonUtil;
import com.mguo.backend.common.MguoException;
import com.mguo.backend.common.PastUtil;
import com.mguo.backend.common.Response;
import com.mguo.backend.config.BaseConfig;
import com.mguo.backend.model.FileResouseDO;
import com.mguo.backend.model.QueueNumDTO;
import com.mguo.backend.service.AudioService;
import com.mguo.backend.service.DeptInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/mguo/audio")
@Slf4j
@Api(tags = {"播放接口"})
public class BackendController {

    @Resource
    private AudioService audioService;

    @Resource
    private DeptInfoService deptInfoService;

    @ApiOperation(value = "更新最大的播放列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @GetMapping("/updateMaxQueueSize")
    public Response updateMaxQueueSize(@RequestParam("maxSize") int maxSize){
        try {
            return Response.builderSuccess(audioService.updateMaxQueueSize(maxSize));
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }



    @ApiOperation(value = "获取播放列表数量", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @GetMapping("/getQueueSize")
    public Response getQueueSize(){
        try {
            QueueNumDTO queueNumDTO = audioService.getQueueSize();
            return Response.builderSuccess( "队列最大数量: " + queueNumDTO.getMaxQueueSize() + " ; 当前列表数量: " + queueNumDTO.getNowQueueSize());
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

    @ApiOperation(value = "获取签名", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping("/getSignature")
    public Response getSignature(String url){
        try {
            return Response.builderSuccess(PastUtil.getParam(url));
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

    @ApiOperation(value = "获取播放列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping("/playList")
    public Response<Map<String, Object>> playList(){
        try {
            return Response.builderSuccess(audioService.playList());
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

    @ApiOperation(value = "点播", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping("/dianbo")
    public Response dianbo(@RequestBody Map parm ){
        try {
            String userId = parm.get("userId").toString();
            Long resourceId = Long.parseLong(parm.get("resourceId").toString());
            audioService.addAudio(userId, resourceId);
            return Response.builderSuccess("");
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

    @ApiOperation(value = "停止播放", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping("/stopAudio")
    public Response stopAudio(@RequestBody Map parm){
        try {
            String userId = parm.get("userId").toString();
            Long resourceId = Long.parseLong(parm.get("resourceId").toString());
            FileResouseDO audio = audioService.currentPlayingAudio();
            if(audio.getId().equals(resourceId)){
                if(!audio.getPlayUserId().equals(userId) && !(BaseConfig.adminUser.equals(userId)) ){
                    throw new MguoException("当前音频不是您点播的，不能停止播放");
                }else {
                    audioService.endAudio(resourceId);
                }
            }else{
                return Response.builderSuccess("");
            }
            return Response.builderSuccess("");
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }

    }

    @ApiOperation(value = "移除点播", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping("/cancelAudio")
    public Response cancelAudio(@RequestBody Map parm ){
        try {
            String userId = parm.get("userId").toString();
            Long resourceId = Long.parseLong(parm.get("resourceId").toString());
            audioService.removeAudio(userId,resourceId);
            return Response.builderSuccess("");
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

    @ApiOperation(value = "微信分享", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping("/getWXShare")
    public Response getWXShare(){
        try {
            Map<String,String> map = new HashMap<>();
            map.put("title","青春向党 百年如歌——我的青春在芒果");
            map.put("subtitle","听芒果青年的青春故事");
            map.put("url","https://waibaoh5.tingdao.com/fedd3f4bd3d7b3fa6919346d6bea258c.jpg");
            map.put("targetUrl","https://mangoytree.tingdao.com/pindao.html?scene=49");
            return Response.builderSuccess(map);
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

    @ApiOperation(value = "清空播放列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping("/empty")
    public Response empty(String pwd){
        try {
            if("mongo996".equals(pwd)) {
                audioService.empty();
            }
            return Response.builderSuccess("");
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }


    @ApiOperation(value = "获取openId", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponse(code = 200, message = "success")
    @PostMapping("/getOpenId")
    public Response getOpenId(String code){
        try {
            return Response.builderSuccess(CommonUtil.getOpenId(code));
        } catch (MguoException e) {
            log.error(e.getMessage(), e);
            return Response.builderFail(e.getCode(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Response.builderFail("MG_1001", "系统异常");
        }
    }

}
