package com.mguo.backend.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * DO 实体类
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class DeptInfoDO extends BaseDo {

	/**
     * 主键
     */
	private Long id;

	/**
     * 公司名
     */
	private String companyName;

	/**
     * 部门名
     */
	private String deptName;

}
