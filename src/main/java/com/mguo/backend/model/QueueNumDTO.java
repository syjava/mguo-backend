package com.mguo.backend.model;

import lombok.Data;

/**
 * 类QueueNumDto的实现描述：TODO 类实现描述
 *
 * @author sunyj 2022/4/28 0:29
 */
@Data
public class QueueNumDTO {

    /**
     * 最大队列数量
     */
    private Integer maxQueueSize;

    /**
     * 当前数量
     */
    private Integer nowQueueSize;
}
