package com.mguo.backend.model;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * DO 实体类
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class FileResouseDO extends BaseDo {

	/**
     * 主键
     */
	private Long id;

	/**
     * 资源名
     */
	private String resourceName;

	/**
     * 资源URL
     */
	private String resourceUrl;

	/**
     * 资源时长
     */
	private Integer resourceTime;

	/**
     * 单位（公司）
     */
	private String companyName;

	/**
	 * 部门
	 */
	private String deptName;

	/**
	 * 资源类型
	 */
	private String resourceType;

	/**
	 * 是否可以播放
	 */
	private Boolean canPlay;

	/**
	 * 排队数量
	 */
	private int  queueCount;


	/**
	 * 开始时间
	 */
	private Date startTime;

	/**
	 * 点播者
	 */
	private String playUserId;
	/**
	 * 播放了多长时间
	 */
	private int duration;

}
