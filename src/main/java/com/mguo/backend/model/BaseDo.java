package com.mguo.backend.model;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 类BaseDo的实现描述：TODO 类实现描述
 *
 * @author sunyj 2022/4/26 19:24
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class BaseDo {


    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 创建人
     */
    private Long creator;
    /**
     * 更新人
     */
    private Long modifier;

    /**
     * 是否逻辑删除：0-否，1-是
     */
    private Byte isDeleted;
}
