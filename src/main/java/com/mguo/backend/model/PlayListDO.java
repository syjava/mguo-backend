package com.mguo.backend.model;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * DO 实体类
 * 
 * @author xianghaitao
 * @date 2022-04-26
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class PlayListDO extends BaseDo {

	/**
     * 主键
     */
	private Long id;

	/**
     * 资源id
     */
	private Long resourceId;

	/**
     * 部门信息id
     */
	private Long deptInfoId;

	/**
     * 播放状态:  1: 待播放 2: 播放中   3: 播放完成
     */
	private Integer playStatus;

	/**
     * 播放开始时间
     */
	private Date playStartTime;

	/**
     * 播放结束时间
     */
	private Date playEndTime;

	/**
     * 异常结束时间
     */
	private Date playExcEndTime;

	/**
     * 点播者
     */
	private String playerName;

}
