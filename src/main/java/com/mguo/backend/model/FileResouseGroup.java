package com.mguo.backend.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * TODO
 *
 * @Author: Nyj
 * @Date: 2022/4/26 22:19
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class FileResouseGroup {
    /**
     * 部门
     */
    private String companyName;

    /**
     * 公司名称
     */
    private String deptName;

    /**
     * 资源
     */
    private List<FileResouseDO> fileResouseDOs;
}
